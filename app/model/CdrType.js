/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.model.CdrTypeModel', {
    extend: 'Ext.data.Model',
    fields: ['cdrType', 'count'],

    proxy: {
        type: 'ajax',
        url: 'data/stations.json',
        reader: {
            type: 'json',
            root: 'results'
        }
    }
});