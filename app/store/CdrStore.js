/**
 * Created by Samira on 8/12/17.
 */
Ext.define('cdrReport.store.CdrStore', {
    extend: 'Ext.data.Store',
    requires: 'cdrReport.model.CdrModel',
    model: 'cdrReport.model.CdrModel'
});