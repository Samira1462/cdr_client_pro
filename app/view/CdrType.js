/**
 * Created by Samira on 8/13/17.
 */

Ext.define('cdrReport.view.CdrType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.cdrtype',

    store: 'CdrTypeStore',

    requires: [
        'Ext.grid.GridPanel',
        'Ext.button.Button',
        'Ext.form.TextField'
    ],

    layout: 'fit',

    id: 'cdrtype',

    initComponent: function () {
        this.items = [
            {
                xtype: 'panel',
                width: 600,
                height: 600,
                items: [
                    {
                        xtype: 'menu',
                        width: 150,
                        height: 150,
                        floating: false,
                        margin: '10 5 3 10',
                        items: [
                            {
                                xtype: 'menucheckitem',
                                text: 'ATTACH'
                            },
                            {
                                xtype: 'menucheckitem',
                                text: 'DETACH'
                            },
                            {
                                xtype: 'menucheckitem',
                                text: 'REGISTER'
                            },
                            {
                                xtype: 'menucheckitem',
                                text: 'ENTER'
                            },
                            {
                                xtype: 'menucheckitem',
                                text: 'EXIT'
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        width: 100,
                        height: 30,
                        margin: '10 5 3 120',
                        handler: function () {
                        }
                    }
                ]
            },
            {
                xtype: 'panel',
                items: [
                    {
                        xtype: 'grid',
                        title: 'CDR TYPE RESULT',
//                        store: Ext.getStore('cdrTypeStore'),
                        columns:[{
                            text: "type",
                            width: 170,
                            flex:1,
                            dataIndex: 'cdrType',
                            sortable: true
                        },{
                            text: "count",
                            width: 160,
                            flex:1,
                            dataIndex: 'count',
                            sortable: true
                        }]
                    }
                ]
            }
        ];

        this.callParent();
    }


});
