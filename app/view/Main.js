Ext.define('cdrReport.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'cdrReport.view.CdrType',
        'cdrReport.view.Cdr'
    ],

    xtype: 'app-main',

    layout: {
        type: 'border'
    },

    items: [{
        region: 'center',
        xtype: 'tabpanel',
        items :[{
            title: 'CDR Type Report',
            xtype: 'cdrtype'
        },
        {
            title:'CDR Report',
            xtype: 'cdr'
        }
        ]
    }]
});