Ext.define('cdrReport.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.layout.container.Fit',
        'cdrReport.view.Main'
    ],

//    rtl: true,

    layout: {
        type: 'fit'
    },

    items: [{
        xtype: 'app-main'
    }]
});
